## Реализоать REST API сервис со следующим функционалом

1. Должен принимать POST запрос по протоколу JSON-RPC 2.0, в котором в поле `params` передается JSON: 
```json
{ 
    "event_timestamp": UInt32
    "uid": "String",
    "search_text": "String",
    "long": Float32,
    "lat": Float32,
    "search_result": {"search_engine":"String","values":["String","String","String","String"]}
}
```
Пример тела POST запроса :
```json
{"json-rpc":2.0, "id":123,"method":"save_to_ch", "params":
    { 
        "event_timestamp": 1535068800,
        "uid": "uid_123",
        "search_text": "Тестовы запрос \t в базу КХ\n",
        "long": 55.1111,
        "lat": 33.2222,
        "search_result": {"search_engine":"Test","values":["prod_uid_1","prod_uid_2","prod_uid_3","prod_uid_4"]}
    }
}
```
Сервис должен принимать до 1000 запросов в секунду


2. каждый запрос необходимо сохранять в Clickhouse в таблицу `test`, которая имеет следующие поля:
- event_date Date
- uid String 
- search_text String
- long Float32,
- lat Float32
- search_result String

Запись в базу КликХауз необходимо делать обычным HTTP POST запросом на порт 8123 в базу `default` КликХауза. 

Пример `url` для запроса:
`http://localhost:8123/default?query=INSERT%20INTO%20test%20FORMAT%20TSVWithNamesAndTypes`
В теле запроса передается текст, представленынй в формате TSV - резделитель значений `\t`, разделитель строк `\n`.   
Пример:
```java
String a = """
event_timestamp\tuid\tsearch_text\tlong\tlat\tsearch_result\n 
2018-09-01\tuid_123\tТестовы запрос \\t в базу КХ\\n\t55.1111\t33.2222\t{"search_engine":"Test","values":["prod_uid_1","prod_uid_2","prod_uid_3","prod_uid_4"]}\n
"""
```
3. Предусмотреть возможность использования перемнных окружения для конфигурации. Например, задать домен сервера, где расположен КликХауз через переменную окружения CLICHOUSE_ADDR

4. Учесть ограничение на количество запросов в КликХауз в секунду
Сервис не должен делать более 100 запросов в КликХауз в независимости от того, сколько сообщений ему передается по РЕСТ АПИ. 

5. Учесть экранирование спец символов в поле `search_result`


Логика работы севриса следующая:  
Клиентом отправляется на REST API сервиса POST запрос.
Сервер валидирует данные на тему `JSON-RPC 2.0` и если все правильно, то берет данные из `params` в работу и параллельно возвращает ответ `Ok` на запрос. Таким образом, клиент, который отправляет запрос на сервис, не ждет, когда сервис закончит обработку данных. Далее, севрис сохраняет полученные от клиентов данные в таблицу КликХауза.


## Дополнительная информация
Документация по КликХауз: 
- https://clickhouse.yandex/docs/ru/getting_started/  
- https://clickhouse.yandex/docs/ru/interfaces/http_interface/  

Запрос на создание таблицы `test`
```SQL
CREATE TABLE default.test (
 event_date Date,
 uid String,
 search_text String,
 long Float32,
 lat Float32,
 search_result String
) ENGINE = MergeTree(
  event_date,
  (
    event_date,
    uid  ),
  8192)
  
```




