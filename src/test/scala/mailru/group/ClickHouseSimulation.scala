package mailru.group

import io.gatling.app.Gatling
import io.gatling.core.Predef._
import io.gatling.core.config.GatlingPropertiesBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._

class ClickHouseSimulation extends Simulation {
  val httpConfig = http.baseURL("http://localhost:8888")

  def body(userId: Long) =
    s"""{"jsonrpc":"2.0","id":$userId,"method":"save_to_ch","params":{"event_timestamp":1535068800,"uid": "uid_123","search_text": "Тестовый запрос","long": 55.1111,"lat": 33.2222,"search_result": {"search_engine":"Test"}}}"""


  def clickHouseRegistry =
    scenario("Click House Simulation")
      .exec(
        http("Upload to Click House")
          .post("")
          .body(StringBody(session => body(session.userId)))
          .asJSON
          .header("Content-Type", "application/json")
      )

  setUp(
    clickHouseRegistry
      .inject(constantUsersPerSec(1000) during(3 seconds))
      .protocols(httpConfig)
  )
}

object Engine extends App {
  val props = new GatlingPropertiesBuilder
  props.simulationClass("mailru.group.ClickHouseSimulation")

  Gatling.fromMap(props.build)
}

