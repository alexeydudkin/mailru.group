package mailru.group

import akka.actor.Actor.Receive
import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import mailru.group.bl.ClickHouseWorker
import mailru.group.bl.ClickHouseWorker.UploadToClickHouse
import mailru.group.messages.{ClickHouseParams, ClickHouseRequest, MethodType}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import spray.json.JsObject
import scala.concurrent.duration._

class ClickHouseWorkerSpec extends TestKit(ActorSystem("Click-House-Spec"))
  with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {

  "Click House Worker" must {
    "throttle incoming requests (not more than 100 per second)" in {
      val probe = TestProbe()

      case object Tick

      def testTickPF: Receive = {
        case UploadToClickHouse =>
          probe.ref ! Tick
      }

      val clickHouseWorker = system.actorOf(Props(new ClickHouseWorker(){
        override def requestProcessing(requests: Stream[ClickHouseRequest]): Receive =
          testTickPF orElse super.requestProcessing(requests)
      }))

      val maxRequestCount = 1000
      val maxMsgCount = 100

      for (_ <- 0 to maxRequestCount) {
        clickHouseWorker ! ClickHouseRequest("2.0", 123, MethodType.save_to_ch, ClickHouseParams(1535068800,"000","text",55.1111F,33.2222F,JsObject.empty))
      }

      val oneSecondMsgCount = probe.receiveWhile(1 second) {
        case Tick => "receive"
      }.size

      val allMsgCount = probe.receiveWhile(10 seconds) {
        case Tick => "receive"
      }.size

      allMsgCount shouldBe maxRequestCount
      oneSecondMsgCount should be < maxMsgCount
    }
  }

  override protected def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
}
