package mailru.group.routes

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import mailru.group.messages.{ClickHouseRequest, ClickHouseResponse}
import mailru.group.utils.ApplicationConfig

class ClickHouseRoute(worker: ActorRef) extends RouteService {

  override def routes: Route =
    post {
      entity(as[ClickHouseRequest]) { request =>
        validate(request.jsonrpc == ApplicationConfig.jsonrpcVersion, "Wrong json rpc version, must be 2.0") {
          worker ! request
          complete(ClickHouseResponse(id = request.id))
        }
      }
    }
}
