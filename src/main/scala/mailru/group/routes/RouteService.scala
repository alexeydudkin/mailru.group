package mailru.group.routes

import akka.http.scaladsl.server.Route
import mailru.group.utils.JsonSupport

trait RouteService extends JsonSupport {
  def routes: Route
}
