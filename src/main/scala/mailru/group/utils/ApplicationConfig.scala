package mailru.group.utils

import com.typesafe.config.{Config, ConfigFactory}

import scala.language.implicitConversions
import collection.JavaConverters._


object ApplicationConfig {
  import scala.collection.JavaConversions._

  val conf: Config = ConfigFactory.load().getConfig("mailrugroup-server")

  val host: String = conf.getString("http.host")
  val port: Int = conf.getInt("http.port")

  val jsonrpcVersion = conf.getString("http.jsonrpc-version")

  val clickHouseAddress = conf.getString("clickhouse.address")
  val clickHouseDatabase = conf.getString("clickhouse.database")
  val clickHouseTable = conf.getString("clickhouse.table")
  val clickHouseFormat = conf.getString("clickhouse.format")

  val clickHouseColumnNames = conf.getStringList("clickhouse.column.names").toList

  val clickHouseChunkSize = conf.getInt("clickhouse.chunk-size")
  val clickHousePeriod = conf.getInt("clickhouse.period")
}
