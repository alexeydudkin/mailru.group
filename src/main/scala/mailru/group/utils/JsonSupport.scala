package mailru.group.utils

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import mailru.group.messages.MethodType.MethodType
import mailru.group.messages._
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object methodTypeJsonFormat extends JsonFormat[MethodType] {
    override def read(json: JsValue): MethodType = json match {
      case JsString(v) => MethodType.withName(v)
      case v => spray.json.deserializationError(s"Expected JsString, but got $v")
    }

    override def write(obj: MethodType): JsValue = JsString(obj.toString)
  }

  implicit val clickHouseParamsFormat = jsonFormat6(ClickHouseParams)
  implicit val clickHouseRequestFormat = jsonFormat4(ClickHouseRequest)
  implicit val clickHouseResponseFormat = jsonFormat3(ClickHouseResponse)
}

object JsonSupport extends JsonSupport
