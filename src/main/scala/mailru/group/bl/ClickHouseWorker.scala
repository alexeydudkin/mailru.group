package mailru.group.bl

import java.text.SimpleDateFormat
import java.util.Date
import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.typesafe.scalalogging.LazyLogging
import mailru.group.messages.{ClickHouseFormat, ClickHouseParams, ClickHouseRequest, MethodType}
import mailru.group.utils.ApplicationConfig
import org.apache.commons.text.StringEscapeUtils
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Try

class ClickHouseWorker extends Actor with LazyLogging {

  import ClickHouseWorker._

  implicit val ec: ExecutionContext = context.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()

  override def preStart(): Unit =
    context
      .system
      .scheduler
      .schedule(0.seconds, ApplicationConfig.clickHousePeriod.millis, self, UploadToClickHouse)

  override def receive: Receive = requestProcessing()

  def requestProcessing(requests: Stream[ClickHouseRequest] = Stream.empty): Receive = {
    case UploadToClickHouse if requests.nonEmpty =>
      logger.debug("Upload next batch to Click House")

      context.become(requestProcessing(requests.drop(ApplicationConfig.clickHouseChunkSize)))

      uploadToClickHouse(requests.take(ApplicationConfig.clickHouseChunkSize))

    case request: ClickHouseRequest if request.method == MethodType.save_to_ch =>
      logger.debug("Receive new ClickHouse request")
      context.become(requestProcessing(requests :+ request))

    case request: ClickHouseRequest =>
      logger.error("Not implemented query for method {}", request.method)
  }

  private def uploadToClickHouse(requests: Stream[ClickHouseRequest]): Unit = {
    val clickHouseUri = Uri(ApplicationConfig.clickHouseAddress)

    val connectionFlow =
      Http(context.system)
        .outgoingConnection(clickHouseUri.authority.host.address(), clickHouseUri.authority.port)

    Source.single(buildUploadRequest(clickHouseUri, requests))
      .via(connectionFlow)
      .runWith(Sink.ignore)
  }

  private def buildUploadRequest(uri: Uri, request: Stream[ClickHouseRequest]): HttpRequest = {
    import Uri._

    val query =
      s"INSERT INTO ${ApplicationConfig.clickHouseTable} FORMAT ${ApplicationConfig.clickHouseFormat}"

    HttpRequest(
      HttpMethods.POST,
      Uri(ApplicationConfig.clickHouseAddress)
        .withPath(Path.Slash(Path(ApplicationConfig.clickHouseDatabase)))
        .withQuery(Query("query" -> query)),
      entity = prepareRequestEntity(request.map(_.params))
    )
  }

  private def prepareRequestEntity(params: Stream[ClickHouseParams]): String = {
    val formattedData = params.map{ param =>
      import param._
      val data = Map(
        {
          val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
          "event_date" -> dateFormat.format(new Date(event_timestamp))
        },
        "uid" -> uid,
        "search_text" -> escapeWithCyrillic(search_text),
        "long" -> long,
        "lat" -> lat,
        "search_result" -> escapeWithCyrillic(search_result.toString())
      )

      ApplicationConfig.clickHouseColumnNames.map(data).mkString("\t")
    }

    Try(ClickHouseFormat.withName(ApplicationConfig.clickHouseFormat))
      .getOrElse(ClickHouseFormat.TabSeparated) match {
      case ClickHouseFormat.TSVWithNames =>
        (ApplicationConfig.clickHouseColumnNames.mkString("\t") +: formattedData).mkString("\n")

      case ClickHouseFormat.TabSeparated =>
        formattedData.mkString("\n")
    }
  }

  private def escapeWithCyrillic(input: String) = {
    val unicodeR = "\\\\u[A-Fa-f\\d]{4}".r

    unicodeR
      .replaceAllIn(
        StringEscapeUtils.escapeJava(input),
        matches => StringEscapeUtils.unescapeJava(matches.group(0))
      )
  }

}

object ClickHouseWorker {

  case object UploadToClickHouse

  def props = Props[ClickHouseWorker]
}
