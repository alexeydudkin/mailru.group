package mailru.group.messages

import spray.json.JsObject

case class ClickHouseRequest(
  jsonrpc: String,
  id: Int,
  method: MethodType.Value,
  params: ClickHouseParams

)

case class ClickHouseParams(
  event_timestamp: Int,
  uid: String,
  search_text: String,
  long: Float,
  lat: Float,
  search_result: JsObject
)
