package mailru.group.messages

case class ClickHouseResponse(
  jsonrpc: String = "2.0",
  id: Int,
  result: Int = 0
)
