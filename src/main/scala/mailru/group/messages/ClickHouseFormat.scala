package mailru.group.messages

object ClickHouseFormat extends Enumeration {
  type ClickHouseFormat = Value

  val TSVWithNames, TabSeparated = Value
}
